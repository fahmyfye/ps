<?php

use App\Http\Controllers\BannerController;
use App\Http\Controllers\ShopperController;
use App\Http\Controllers\PromotionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1'], function () {
    // Banner
    Route::group(['prefix' => 'banner'], function () {
        Route::get('/', [BannerController::class, 'index']);
    });

    // Shopper
    Route::group(['prefix' => 'shopper'], function () {
        Route::get('/', [ShopperController::class, 'index']);
        Route::get('/{id}', [ShopperController::class, 'view']);
    });

    // Promotion
    Route::group(['prefix' => 'promotion'], function () {
        Route::get('/', [PromotionController::class, 'index']);
        Route::get('/{id}', [PromotionController::class, 'view']);
    });
});
