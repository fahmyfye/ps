<?php

namespace App\Http\Controllers;

use App\Models\Promotion;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    public function index()
    {
        $data = Promotion::where('published', 1)
            ->where('close', 0)
            ->where('start', '<=', Carbon::now())
            ->where('end', '>=', Carbon::now())
            ->with('profile')
            ->paginate(10);

        return response()->json(['status' => 'success', 'result' => $data], 200);
    }

    public function view($id)
    {
        $data = Promotion::where('id', $id)
            ->where('close', 0)
            ->with('profile')
            ->first();

        return response()->json(['status' => 'success', 'result' => $data], 200);
    }
}
