<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Shopper;

class ShopperController extends Controller
{
    public function index()
    {
        $data = Shopper::where('active', 1)->with('profile')->get();
        return response()->json(['status' => 'success', 'data' => $data], 200);
    }

    public function view($id)
    {
        $data = Shopper::where('id', $id)->with('profile')->first();
        return response()->json(['status' => 'success', 'data' => $data], 200);
    }
}
