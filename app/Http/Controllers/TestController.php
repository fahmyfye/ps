<?php

namespace App\Http\Controllers;

use App\Models\Promotion;
use Illuminate\Http\Request;

use App\Models\Shopper;

class TestController extends Controller
{
    public function index()
    {
        $shopper = Shopper::where('id', 1)->get();
        $promotion = Promotion::all();
        $data = [
            'shopper' => $shopper,
            'promotion' => $promotion,
        ];
        return response()->json(['status' => 'success', 'data' => $data], 200);
    }
}
