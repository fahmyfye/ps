<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Banner;

class BannerController extends Controller
{
    public function index()
    {
        $data = Banner::where('active', 1)->orderBy('sort')->get();
        return response()->json(['status' => 'success', 'result' => $data], 200);
    }
}
