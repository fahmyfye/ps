<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shopper extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'email', 'firebase_uid', 'sign_in_provider', 'avatar', 'active',
    ];

    public function profile()
    {
        return $this->hasOne(ShopperProfile::class);
    }
}
