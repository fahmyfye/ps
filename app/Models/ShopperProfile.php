<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShopperProfile extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'shopper_id', 'name', 'shop_name', 'avatar', 'phone_no',
    ];
}
