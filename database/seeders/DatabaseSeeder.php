<?php

namespace Database\Seeders;

use App\Models\Shopper;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Shopper::factory(10)->create();
        $this->call([
            ShopperProfileSeeder::class,
            PromotionSeeder::class,
            BannerSeeder::class,
        ]);
    }
}
