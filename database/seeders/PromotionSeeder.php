<?php

namespace Database\Seeders;

use App\Models\Promotion;
use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;

class PromotionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Promotion::insert([
            [
                'shopper_id'  => 1,
                'name'        => 'Promo at IKEA',
                'location'    => 'IKEA',
                'state'       => 'Kuala Lumpur',
                'start'       => Carbon::now(),
                'end'         => Carbon::now()->addHours(4),
                'published'   => 1,
                'close'       => 0,
            ],
            [
                'shopper_id'  => 2,
                'name'        => 'Promo at KLIA 2',
                'location'    => 'KLIA 2',
                'state'       => 'Selangor',
                'start'       => Carbon::now(),
                'end'         => Carbon::now()->addHours(4),
                'published'   => 1,
                'close'       => 0,
            ],
            [
                'shopper_id'  => 3,
                'name'        => 'Promo at Mitsui',
                'location'    => 'Mitsui',
                'state'       => 'Selangor',
                'start'       => Carbon::now(),
                'end'         => Carbon::now()->addHours(4),
                'published'   => 1,
                'close'       => 0,
            ],
            [
                'shopper_id'  => 4,
                'name'        => 'Promo at GPO',
                'location'    => 'GPO',
                'state'       => 'Pahang',
                'start'       => Carbon::now(),
                'end'         => Carbon::now()->addHours(4),
                'published'   => 1,
                'close'       => 0,
            ],
            [
                'shopper_id'  => 5,
                'name'        => 'Promo at AEON',
                'location'    => 'AEON',
                'state'       => 'Selangor',
                'start'       => Carbon::now(),
                'end'         => Carbon::now()->addHours(4),
                'published'   => 1,
                'close'       => 0,
            ],
        ]);
    }
}
