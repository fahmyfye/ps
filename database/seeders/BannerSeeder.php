<?php

namespace Database\Seeders;

use App\Models\Banner;
use Illuminate\Database\Seeder;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Banner::insert([
            [
                'name'   => 'Banner 1',
                'path'   => 'https://i.picsum.photos/id/10/600/300.jpg?hmac=iMHIhXAglBFZDJOiPTzBrGdp9GQb7EkZNKDpFo9hew8',
                'active' => 1,
                'sort'   => 1,
            ],
            [
                'name'   => 'Banner 2',
                'path'   => 'https://i.picsum.photos/id/1015/600/300.jpg?hmac=_2Ho9FLQHp9gYSt4GA6k2-WraxJepChcDezstPSy4Zo',
                'active' => 1,
                'sort'   => 2,
            ],
            [
                'name'   => 'Banner 3',
                'path'   => 'https://i.picsum.photos/id/1018/600/300.jpg?hmac=0hh-R3HUJVhnWU9SZGbUZ80J1Djs3EZLwPdpfmHY8Pk',
                'active' => 1,
                'sort'   => 3,
            ],
            [
                'name'   => 'Banner 4',
                'path'   => 'https://i.picsum.photos/id/1045/600/300.jpg?hmac=Z8ziFYy3cGWS1196cBBM5B6mbQOIjshpjP5FfVA8ELc',
                'active' => 1,
                'sort'   => 4,
            ],
            [
                'name'   => 'Banner 5',
                'path'   => 'https://i.picsum.photos/id/1050/600/300.jpg?hmac=SAxIp9fS1reNik41rPwAJqUn080x3y0fOVPIXgvDZxQ',
                'active' => 1,
                'sort'   => 5,
            ],
        ]);
    }
}
