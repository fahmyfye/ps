<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ShopperProfile;

class ShopperProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ShopperProfile::insert([
            [
                'shopper_id' => 1,
                'name'       => 'Lonni Hartell',
                'shop_name'  => 'Pixoboo',
                'avatar'     => 'https://i.picsum.photos/id/250/300/300.jpg?hmac=mZh9RQOu46-RyeW5J7Jhgjt9pbthzGC7l7Uy9XhFjYk',
                'phone_no'   => '+60102223311',
            ],
            [
                'shopper_id' => 2,
                'name'       => 'Klara Bern',
                'shop_name'  => 'Gigabox',
                'avatar'     => 'https://i.picsum.photos/id/203/300/300.jpg?hmac=6IV-xNj_b0zmc0InlHbs23VYvUky2xCitu-hYsGr1wo',
                'phone_no'   => '+60102223322',
            ],
            [
                'shopper_id' => 3,
                'name'       => 'Leupold Tedorenko',
                'shop_name'  => 'Jaxworks',
                'avatar'     => 'https://i.picsum.photos/id/211/300/300.jpg?hmac=OF9bA9rOoAjjijVHeO1HnB5BovdB15D3BRnioEDOR88',
                'phone_no'   => '+60102223333',
            ],
            [
                'shopper_id' => 4,
                'name'       => 'Culley Offill',
                'shop_name'  => 'Voonte',
                'avatar'     => 'https://i.picsum.photos/id/219/300/300.jpg?hmac=dmonesrXdE-9PMrYQcQn9va1iu5a_0p4wGyhw93QSSA',
                'phone_no'   => '+60102223344',
            ],
            [
                'shopper_id' => 5,
                'name'       => 'Carmelita Weymont',
                'shop_name'  => 'Jamia',
                'avatar'     => 'https://i.picsum.photos/id/210/300/300.jpg?hmac=TDUC8sCqlplTSd18Th1up8a8BCu0JeA_VTH1G1D6Thg',
                'phone_no'   => '+60102223355',
            ],
            [
                'shopper_id' => 6,
                'name'       => 'Cthrine Warnock',
                'shop_name'  => 'Eazzy',
                'avatar'     => 'https://i.picsum.photos/id/249/300/300.jpg?hmac=x8NonWWcc5-vRhb8a_84Fz9GU35NS2L8bF6Lcw6aIoQ',
                'phone_no'   => '+60102223366',
            ],
            [
                'shopper_id' => 7,
                'name'       => 'Bernarr Singyard',
                'shop_name'  => 'Riffwire',
                'avatar'     => 'https://i.picsum.photos/id/29/300/300.jpg?hmac=7V5Tl7xSnR3G7AHTEUIf7U3-gKgWbdX5cYzqh1W_6ZQ',
                'phone_no'   => '+60102223377',
            ],
            [
                'shopper_id' => 8,
                'name'       => 'Doe Shellshear',
                'shop_name'  => 'Tagcat',
                'avatar'     => 'https://i.picsum.photos/id/3/300/300.jpg?hmac=RT2JK6MzdIgNIWoIj61uPcz8aOSOi3lu2vhnwOxs7lY',
                'phone_no'   => '+60102223388',
            ],
            [
                'shopper_id' => 9,
                'name'       => 'Logan Cough',
                'shop_name'  => 'Devbug',
                'avatar'     => 'https://i.picsum.photos/id/307/300/300.jpg?hmac=8B1OjGHHyab52NEiBZTWu-dUZ5Ip7mZ_DGrm4S5Nqts',
                'phone_no'   => '+60102223399',
            ],
            [
                'shopper_id' => 10,
                'name'       => 'Milton Josskowitz',
                'shop_name'  => 'Dynabox',
                'avatar'     => 'https://i.picsum.photos/id/304/300/300.jpg?hmac=G0uliBCivPslLUmoPIL5MvjxOfbM9nSZBMv5sYD-6DM',
                'phone_no'   => '+60102223300',
            ],
        ]);
    }
}
